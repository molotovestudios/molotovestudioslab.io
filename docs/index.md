# GEP da Molotov Estúdios

Seja bem vinda, pessoa desenvolvedora! Se está vendo isso, provavelmente é por que você faz parte da Molotov Estúdios. Os passos para estruturar um projeto da Molotov Estúdios estão aqui. Sinta-se livre para consultar esse guia sempre que tiver dúvida sobre como inicializar e/ou organizar um projeto da casa.

Nesse arquivo, você encontrará

* Ferramentas usadas pela Molotov Estúdios
* Primeiros passos para criação do projeto
* Organização de pastas do projeto
* Organização do código do projeto
* Organização das Cenas (Scenes) do projeto
