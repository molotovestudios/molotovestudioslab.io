# Organização do código do projeto

Ao criar um código num projeto da Molotov, coloque o nome do arquivo (a classe) em inglês. Essa língua também será utilizada para nomear quaisquer outros atributos e métodos dos arquivos. 

A seguir, utilize a seguinte estrutura para os códigos: 

1. `Using`.
2. Constantes `public const`, `protected const`, `private const`.
3. Atributos `public`, atributos `protected`, atributos `private`.
5. Enums `public enums`, `protected enums`, `private enums`.
6. Propriedades (getters and setters do C#).
7. Eventos.
8. Métodos `public`, métodos `protected`, métodos `private`.

Dentre todas essas categorias, os elementos `static` vem acima dos elementos não `static`. Por fim, como último critério, os `read-only` vem acima dos não `read-only`. É importante frisar que os atributos `private` com `[SerializeField]` vem acima dos demais atributos `private` sem `[SerializeField]`.

Quanto à declaração dos elementos, segue o seguinte padrão:

1. Constantes em letras maiúsculas separadas por `_` (`EXEMPLO_UM`).
2. Atributos `public` começam com a primeira letra da primeira palavra maiúscula e a primeira letra das demais palavras maiúscula (`exemploDois`).
3. Atributos `private` e `protected` começam com `_`, com a primeira letra da primeira palavra maiúscula e a primeira letra das demais palavras maiúscula (`_exemploTres`).
4. Enums, Propriedades e Métodos tem todas as primeiras letras das suas palavras maiúsculas (`ExemploQuatro`).

IMPORTANTE: não tenha o costume de utilizar atributos `public`. Bad habits! 