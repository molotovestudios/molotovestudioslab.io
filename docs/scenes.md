# Organização das Cenas (Scenes) do projeto

Na interface do Unity existe uma janela chamada hierarquia, onde administramos os objetos que estão na cena (game objects). Podemos criar um sistema de pastas na hierarquia criando objetos vazios com as seguintes propriedades:

* Posição 0, 0, 0.
* Rotação 0, 0, 0.
* Escala 1, 1, 1.

Com as propriedades acima, devemos criar na hierarquia de cada cena os seguintes Game Objects vazios:

* Background: todos os elementos do cenário.
* Play Area: todos os elementos que controlamos ou que interagem com elementos que controlamos.
* Foreground: todos os elementos que ficam na frente da play area e elementos de interface.
* Management: elementos que não possuem recursos visuais, apenas gerenciamento.

