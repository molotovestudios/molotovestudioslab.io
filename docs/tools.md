# Ferramentas usadas pela Molotov Estúdios

A Molotov Estúdios utiliza de algumas ferramentas para produzir jogos, no sistema operacional Windows 10. São elas:

## Unity Engine

Unity é a principal ferramenta usada para produção de jogos da Molotov. É o motor de jogo e a linguagem de programação utilizada nela é C# (C-Sharp). 

Para utilizar a Unity, é preciso um login e senha de usuário. Utilize o login da Molotov. Caso seja solicitado, ative a licença.

## Aseprite

Aseprite é a ferramenta usada para confecção de artes em pixel. Nessa ferramenta são feitos os assets dos jogos.

Utilizamos o Aseprite através de uma conta na Steam. Utilize o login da Molotov. É possível também baixar o Aseprite no-Steam, gratuitamente (google it!).

## Git

Git é o sistema de versionamento que usamos para atualizar nossos projetos num repositório e versioná-los. Usamos o [gitlab](https://gitlab.com/), e o acesso desse site está com um dos lead programmers da Molotov Estúdios (daewglrxD ou Romuboy).

## Ferramentas a serem consideradas 

Considera-se a utilização do Godot Engine no futuro e de distribuições Linux (como Ubuntu).