# Organização das pastas do projeto

Sempre que criamos um projeto do Unity, um conjunto padrão de pastas é criado. Dentre estas pastas está a pasta Assets, onde ficam todos os arquivos que utilizaremos no projeto.

Dentro desta pasta deverão ser criadas mandatoriamente as seguintes pastas (importante o underline para que essas pastas sejam organizadas acima das outras):

* `_scenes`: onde guardaremos as cenas.
* `_scripts`: onde guardaremos os códigos C# desatrelados à entidades.

À medida que o jogo for sendo desenvolvido acontecerá a necessidade de criação de novas pastas. Essas pastas irão conter os elementos que compõem o jogo e deverão ser organizadas com base nas suas entidades. Por exemplo: todos os assets da nave (sprites, animações, scripts etc) de um jogo deverão estar na pasta `nave`. A sugestão de organização é que cada pasta de entidade seja composta pelas seguintes pastas, criadas de acordo com conveniência:

* `animations`: onde guardaremos as animações.
* `materials`: onde guardaremos os arquivos que representam materiais do Unity.
* `prefabs`: onde guardaremos os prefabs (objetos pré-fabricados).
* `scripts`: onde guardaremos os códigos C# desatrelados à entidades.
* `sounds`: onde guardaremos efeitos sonoros e as músicas.
* `sprites`: onde guardaremos arquivos de imagens.
