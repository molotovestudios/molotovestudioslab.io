# Primeiros passos para criação do projeto

Para criar um projeto de um jogo da Molotov, siga os seguintes passos:

1. Crie o projeto na Unity: crie o projeto na Unity e dê à ele o nome (provavelmente de protótipo) do projeto, no seguinte estilo: `nome-do-projeto`.
2. Crie o repositório no Gitlab: crie o repositório do projeto no Gitlab e dê a ele o nome tal como o nome do projeto Unity. Após isso, faça o commit inicial com o projeto Unity na branch "master" e crie a partir dela uma nova branch (nome sugerido: dev).
3. Organize as pastas: no projeto Unity, organize as pastas tal como a Organização de pastas do projeto diz.
4. Utilize a organização de cenas: no projeto Unity, organize a primeira cena tal como a "Organização de Cenas (Scenes) do projeto" diz.
